import { writable, get, type Writable } from 'svelte/store';
import { toast } from '@zerodevx/svelte-toast';
import { fetch as tauri_fetch } from '@tauri-apps/plugin-http';
import { getTauriVersion } from '@tauri-apps/api/app';
import type { Category } from '$lib/bindings/Category';
import type { Meal } from '$lib/bindings/Meal';
import { Err, Ok, type Result } from 'ts-results';
import { MyError, type Try } from '$lib/error';
import type { Ingredient } from '$lib/bindings/Ingredient';
import type { MealIngredient } from '$lib/bindings/MealIngredient';

const tauri: Writable<null | boolean> = writable(null);

export enum Month {
	January = 'January',
	February = 'February',
	March = 'March',
	April = 'April',
	May = 'May',
	June = 'June',
	July = 'July',
	August = 'August',
	September = 'September',
	October = 'October',
	November = 'November',
	December = 'December'
}
export const all_months: Month[] = [
	Month.January,
	Month.February,
	Month.March,
	Month.April,
	Month.May,
	Month.June,
	Month.July,
	Month.August,
	Month.September,
	Month.October,
	Month.November,
	Month.December
];

export class SelectItem {
	name: string;
	value: number;

	constructor(name: string, value: number) {
		this.name = name;
		this.value = value;
	}
}

export async function set_running_with_tauri(): Promise<void> {
	try {
		const version = await getTauriVersion();
		console.log('With tauri', version);
		tauri.set(true);
	} catch {
		console.log('Without tauri');
		tauri.set(false);
	}
}

export async function own_fetch(...args: unknown[]): Promise<Response> {
	while (get(tauri) == null) {
		await new Promise((r) => setTimeout(r, 100));
	}
	args[0] = get_server() + args[0];
	if (get(tauri)) {
		// @ts-expect-error just want my own fetch
		return tauri_fetch(...args);
	} else {
		// @ts-expect-error just want my own fetch
		return fetch(...args);
	}
}

function is_dev(): boolean {
	return import.meta.env.DEV;
}

export function get_server(): string {
	if (get(tauri)) {
		return 'http://localhost:8000';
	} else if (is_dev()) {
		return 'http://localhost:8000';
	} else {
		return window.location.origin;
	}
}

export async function call_backend<Type>(
	method: string,
	path: string,
	body: object | string | null,
	error_message: string,
	no_answer = false
): Promise<Result<Type, MyError>> {
	if (body !== null) {
		body = JSON.stringify(body);
	}
	const response = await own_fetch(path, {
		method: method,
		body: body
	});
	if (!response.ok) {
		const text = await response.text();
		if (error_message.length != 0) {
			toast.push(`${error_message}: ${text}`);
		}
		return Err(new MyError(text));
	}
	if (no_answer) {
		return Ok(null as Type);
	}
	return Ok(await response.json());
}

export async function get_categories(): Promise<Try<Category[]>> {
	return await call_backend('GET', '/category', null, 'Failed to fetch categories');
}

export async function get_meals(month: Month | null = null): Promise<Try<Meal[]>> {
	if (month === null) {
		return await call_backend('GET', '/meal', null, 'Failed to fetch meals');
	} else {
		const index = all_months.findIndex((x) => x == month);
		return await call_backend('GET', `/meal/month/${index}`, null, 'Failed to fetch meals');
	}
}

export async function get_ingredients(): Promise<Try<Ingredient[]>> {
	return await call_backend('GET', '/ingredient', null, 'Failed to fetch ingredients');
}

export async function get_meal_ingredients(meal_id: number): Promise<Try<MealIngredient[]>> {
	return await call_backend(
		'GET',
		`/meal-ingredients/${meal_id}`,
		null,
		'Failed to fetch ingredients'
	);
}

export function get_empty_ingredient(): Ingredient {
	return {
		id: null,
		name: '',
		month_start: 0,
		month_end: 0,
		unit: ''
	};
}

export async function add_ingredient(meal_ingredient: MealIngredient): Promise<Try<void>> {
	return await call_backend(
		'PUT',
		`/meal-ingredient`,
		meal_ingredient,
		'Failed to add ingredient',
		true
	);
}

export async function recompute_seasons(): Promise<Try<void>> {
	return await call_backend(
		'POST',
		'/meal-ingredients/recompute-seasons',
		null,
		'Failed to recompute seasons',
		true
	);
}

export async function try_delete_meal_ingredient(item: MealIngredient): Promise<Try<void>> {
	return await call_backend('DELETE', '/meal-ingredient', item, '', true);
}

export async function pull_data(address: string): Promise<Try<void>> {
	const encoded = encodeURIComponent(address);
	console.log(encoded);
	return await call_backend('POST', `/pull/${encoded}`, null, 'Failed to pull data', true);
}
