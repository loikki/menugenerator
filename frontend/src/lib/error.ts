import type { Result } from 'ts-results';

export class MyError {
	message: string;

	constructor(message: string) {
		this.message = message;
	}
}

export type Try<Type> = Result<Type, MyError>;
