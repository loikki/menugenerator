use std::env;

fn main() {
    // Tauri feature does not work properly => use derived variable
    if env::var("CARGO_FEATURE_TAURI").is_ok() {
        tauri_build::build();
    }
}
