// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

// With tauri
#[cfg(feature = "tauri")]
use menu_generator_lib::start_with_tauri;

#[cfg(feature = "tauri")]
pub fn main() {
    start_with_tauri();
}

// Without tauri
#[cfg(not(feature = "tauri"))]
use rocket::{fs::FileServer, launch};

#[cfg(not(feature = "tauri"))]
use menu_generator_lib::backend;

#[cfg(not(feature = "tauri"))]
#[launch]
async fn rocket() -> _ {
    use menu_generator_lib::config::Config;
    use std::path::PathBuf;

    // Get directories
    let current_dir = PathBuf::from(".");

    let config = Config::new(&current_dir);
    let rocket = backend::get_rocket(current_dir).await;
    if let Ok(rocket) = rocket {
        rocket.mount("/", FileServer::from(config.frontend_path()))
    } else {
        panic!("Failed to start rocket {rocket:?}");
    }
}
