// @generated automatically by Diesel CLI.

diesel::table! {
    category (id) {
        id -> Integer,
        #[max_length = 255]
        name -> Varchar,
    }
}

diesel::table! {
    ingredient (id) {
        id -> Integer,
        #[max_length = 255]
        name -> Varchar,
        month_start -> Unsigned<Tinyint>,
        month_end -> Unsigned<Tinyint>,
        #[max_length = 255]
        unit -> Nullable<Varchar>,
    }
}

diesel::table! {
    meal (id) {
        id -> Integer,
        #[max_length = 255]
        name -> Varchar,
        light -> Bool,
        meat -> Bool,
        month_start -> Unsigned<Tinyint>,
        month_end -> Unsigned<Tinyint>,
        #[max_length = 500]
        source -> Varchar,
        category_id -> Integer,
        recipe -> Text,
    }
}

diesel::table! {
    meal_ingredient (ingredient_id, meal_id) {
        ingredient_id -> Integer,
        meal_id -> Integer,
        quantity -> Float,
    }
}

diesel::joinable!(meal -> category (category_id));
diesel::joinable!(meal_ingredient -> ingredient (ingredient_id));
diesel::joinable!(meal_ingredient -> meal (meal_id));

diesel::allow_tables_to_appear_in_same_query!(
    category,
    ingredient,
    meal,
    meal_ingredient,
);
