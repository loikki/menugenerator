use sea_orm::{ActiveValue::NotSet, Set};
use serde::{Deserialize, Serialize};
use ts_rs::TS;

use crate::entity::meal;

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize, TS, Default)]
#[ts(export)]
pub struct Meal {
    pub id: Option<i32>,
    pub name: String,
    pub light: bool,
    pub meat: bool,
    pub month_start: u8,
    pub month_end: u8,
    pub source: String,
    pub category_id: i32,
    pub recipe: String,
}

impl From<meal::Model> for Meal {
    fn from(value: meal::Model) -> Self {
        Self {
            id: Some(value.id),
            name: value.name,
            light: value.light,
            meat: value.meat,
            month_end: value.month_end,
            month_start: value.month_start,
            source: value.source,
            category_id: value.category_id,
            recipe: value.recipe,
        }
    }
}

impl From<Meal> for meal::ActiveModel {
    fn from(value: Meal) -> Self {
        let id = if let Some(id) = value.id {
            Set(id)
        } else {
            NotSet
        };
        meal::ActiveModel {
            id,
            name: Set(value.name),
            light: Set(value.light),
            meat: Set(value.meat),
            month_end: Set(value.month_end),
            month_start: Set(value.month_start),
            source: Set(value.source),
            category_id: Set(value.category_id),
            recipe: Set(value.recipe),
        }
    }
}
