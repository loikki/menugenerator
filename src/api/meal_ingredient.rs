use sea_orm::Set;
use serde::{Deserialize, Serialize};
use ts_rs::TS;

use crate::entity::meal_ingredient;

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize, TS, Default)]
#[ts(export)]
pub struct MealIngredient {
    pub ingredient_id: i32,
    pub meal_id: i32,
    pub quantity: f32,
}

impl From<meal_ingredient::Model> for MealIngredient {
    fn from(value: meal_ingredient::Model) -> Self {
        Self {
            ingredient_id: value.ingredient_id,
            meal_id: value.meal_id,
            quantity: value.quantity,
        }
    }
}

impl From<MealIngredient> for meal_ingredient::ActiveModel {
    fn from(value: MealIngredient) -> Self {
        Self {
            ingredient_id: Set(value.ingredient_id),
            meal_id: Set(value.meal_id),
            quantity: Set(value.quantity),
        }
    }
}
