use sea_orm::{ActiveValue::NotSet, Set};
use serde::{Deserialize, Serialize};
use ts_rs::TS;

use crate::entity::category;

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize, TS, Default)]
#[ts(export)]
pub struct Category {
    pub id: Option<i32>,
    pub name: String,
}

impl From<category::Model> for Category {
    fn from(value: category::Model) -> Self {
        Self {
            id: Some(value.id),
            name: value.name,
        }
    }
}

impl From<Category> for category::ActiveModel {
    fn from(value: Category) -> Self {
        let id = if let Some(id) = value.id {
            Set(id)
        } else {
            NotSet
        };
        Self {
            id,
            name: Set(value.name),
        }
    }
}
