use sea_orm::{ActiveValue::NotSet, Set};
use serde::{Deserialize, Serialize};
use ts_rs::TS;

use crate::entity::ingredient;

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize, TS, Default)]
#[ts(export)]
pub struct Ingredient {
    pub id: Option<i32>,
    pub name: String,
    pub month_start: u8,
    pub month_end: u8,
    pub unit: String,
}

impl From<ingredient::Model> for Ingredient {
    fn from(value: ingredient::Model) -> Self {
        Self {
            id: Some(value.id),
            name: value.name,
            month_end: value.month_end,
            month_start: value.month_start,
            unit: value.unit,
        }
    }
}

impl From<Ingredient> for ingredient::ActiveModel {
    fn from(value: Ingredient) -> Self {
        let id = if let Some(id) = value.id {
            Set(id)
        } else {
            NotSet
        };
        ingredient::ActiveModel {
            id,
            name: Set(value.name),
            month_start: Set(value.month_start),
            month_end: Set(value.month_end),
            unit: Set(value.unit),
        }
    }
}
