use std::io::Cursor;

use rocket::{
    error,
    fairing::{Fairing, Info, Kind},
    http::Status,
    Request, Response,
};

#[derive(Default)]
pub struct ErrorFairing;

#[rocket::async_trait]
impl Fairing for ErrorFairing {
    fn info(&self) -> Info {
        Info {
            name: "Error Fairing",
            kind: Kind::Response,
        }
    }

    async fn on_response<'r>(&self, request: &'r Request<'_>, response: &mut Response<'r>) {
        if response.status() == Status::InternalServerError {
            let body = response.body_mut();
            let text = body
                .to_string()
                .await
                .unwrap_or_else(|_| String::from("The error message cannot be recovered"));
            error!("{}: {:?}", request, text);
            response.set_sized_body(None, Cursor::new(text));
        }
    }
}
