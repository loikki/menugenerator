use reqwest::Error as ReqError;
use rocket::http::Status;
use rocket::response::Responder;
use rocket::tokio::task::JoinError;
use rocket::{response, Request};
use sea_orm::DbErr;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum MenuError {
    #[error("MenuGenerator error: {0}")]
    Menu(String),

    #[error("MenuGenerator error: {0}")]
    MenuStr(&'static str),

    #[error("Join error: {0}")]
    Join(#[from] JoinError),

    #[error("Failed to process IO: {0}")]
    IOError(#[from] std::io::Error),

    #[error("Failed to convert json: {0}")]
    SerdeError(#[from] serde_json::Error),

    #[error("Failed to handle sql: {0}")]
    SqlError(#[from] DbErr),

    #[error("Failed to handle request: {0}")]
    ReqwestError(#[from] ReqError),
}

impl<'r> Responder<'r, 'static> for MenuError {
    fn respond_to(self, req: &'r Request<'_>) -> response::Result<'static> {
        let string = format!("{}", self);
        let status = Status::InternalServerError;
        response::Response::build_from(string.respond_to(req)?)
            .status(status)
            .ok()
    }
}
