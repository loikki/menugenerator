use rocket::tokio::sync::Mutex;
use rocket::{http::Method, Build, Rocket};
use rocket_cors::{AllowedOrigins, CorsOptions};
use std::net::Ipv4Addr;
use std::path::PathBuf;

use crate::database::Database;

use crate::config::Config;
use crate::error_fairing::ErrorFairing;
use crate::menu_error::MenuError;
use crate::routes::{category, general, ingredient, meal, meal_ingredient};

pub async fn get_rocket(config_dir: PathBuf) -> Result<Rocket<Build>, MenuError> {
    // Config
    let config = Config::new(&config_dir);
    let ip = if config.expose_rocket() {
        Ipv4Addr::new(0, 0, 0, 0)
    } else {
        Ipv4Addr::new(127, 0, 0, 1)
    };

    // Migrate DB
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;
    db.migrate().await?;

    let config: Mutex<Config> = Mutex::new(config);

    let rocket_config = rocket::Config::figment()
        .merge(("address", ip))
        .merge(("log_level", "normal"));

    // Cors
    let cors = CorsOptions::default()
        .allowed_origins(AllowedOrigins::all())
        .allowed_methods(
            vec![
                Method::Get,
                Method::Post,
                Method::Patch,
                Method::Put,
                Method::Delete,
            ]
            .into_iter()
            .map(From::from)
            .collect(),
        )
        .allow_credentials(true);

    Ok(rocket::custom(rocket_config)
        .attach(ErrorFairing)
        .manage(config)
        .mount("/category", category::get_routes())
        .mount("/meal", meal::get_routes())
        .mount("/meal-ingredient", meal_ingredient::get_routes())
        .mount("/", general::get_routes())
        .mount("/ingredient", ingredient::get_routes())
        .attach(cors.to_cors().unwrap()))
}
