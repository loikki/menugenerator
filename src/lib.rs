pub mod api;
pub mod backend;
pub mod config;
pub mod database_trait;
pub mod entity;
pub mod error_fairing;
pub mod menu_error;
mod routes;

pub mod database;

#[cfg(feature = "tauri")]
pub fn start_with_tauri() {
    println!("Starting app");

    use rocket::tokio;
    use std::sync::{Arc, Mutex};
    use tauri::Manager;

    // Start rocket
    let rt = Arc::new(Mutex::new(tokio::runtime::Runtime::new().unwrap()));
    let rt_clone = Arc::clone(&rt);

    // Start tauri
    tauri::Builder::default()
        .plugin(tauri_plugin_store::Builder::default().build())
        .plugin(tauri_plugin_http::init())
        .setup(move |app| {
            let config_dir = app.path().document_dir().expect("Failed to get config dir");

            let rt = rt_clone.lock().unwrap();
            rt.spawn(async {
                let rocket = backend::get_rocket(config_dir).await;
                if rocket.is_err() {
                    eprintln!("Error while starting app: {rocket:?}");
                    std::process::exit(1);
                }
                rocket.unwrap().launch().await
            });
            Ok(())
        })
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}

#[cfg(target_os = "android")]
#[tauri::mobile_entry_point]
fn android_start() {
    start_with_tauri();
}
