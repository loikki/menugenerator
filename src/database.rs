use crate::api::{Category, Ingredient, Meal, MealIngredient};
use crate::database_trait::DatabaseTrait;
use crate::entity::{category, ingredient, meal, meal_ingredient};
use crate::menu_error::MenuError;
use async_trait::async_trait;
use migration::{Migrator, MigratorTrait};
use sea_orm::ActiveModelTrait;
use sea_orm::{
    entity::EntityTrait, ColumnTrait, ConnectOptions, Database as SeaOrmDatabase,
    DatabaseConnection, QueryFilter,
};

pub struct Database {
    db: DatabaseConnection,
}

impl Database {
    pub async fn new(url: &str) -> Result<Self, MenuError> {
        let opt = ConnectOptions::new(url)
            .set_schema_search_path("entity")
            .to_owned();
        Ok(Self {
            db: SeaOrmDatabase::connect(opt).await?,
        })
    }
    pub async fn migrate(&self) -> Result<(), MenuError> {
        Migrator::up(&self.db, None).await?;
        Ok(())
    }

    pub async fn get_category_by_name(&self, name: &str) -> Result<Option<Category>, MenuError> {
        let category = category::Entity::find()
            .filter(category::Column::Name.eq(name))
            .one(&self.db)
            .await?;
        if let Some(category) = category {
            Ok(Some(category.into()))
        } else {
            Ok(None)
        }
    }

    pub async fn get_ingredient_by_name(
        &self,
        name: &str,
    ) -> Result<Option<Ingredient>, MenuError> {
        let ingredient = ingredient::Entity::find()
            .filter(ingredient::Column::Name.eq(name))
            .one(&self.db)
            .await?;
        if let Some(ingredient) = ingredient {
            Ok(Some(ingredient.into()))
        } else {
            Ok(None)
        }
    }
}

#[async_trait]
impl DatabaseTrait for Database {
    // Meal
    async fn get_meals(&self) -> Vec<Meal> {
        let meals = meal::Entity::find()
            .all(&self.db)
            .await
            .expect("Failed to get all the meals");
        meals.iter().map(|x| x.clone().into()).collect()
    }

    async fn add_meal(&self, item: &Meal) -> Result<i32, MenuError> {
        let item: meal::ActiveModel = item.clone().into();
        let mut item = item.save(&self.db).await?;
        Ok(item.id.take().unwrap())
    }

    async fn get_meal_by_name(&self, name: &str) -> Result<Option<Meal>, MenuError> {
        let meal = meal::Entity::find()
            .filter(meal::Column::Name.eq(name))
            .one(&self.db)
            .await?;
        if let Some(meal) = meal {
            Ok(Some(meal.into()))
        } else {
            Ok(None)
        }
    }

    async fn get_meal_by_id(&self, id: i32) -> Result<Option<Meal>, MenuError> {
        let meal = meal::Entity::find_by_id(id).one(&self.db).await?;
        if let Some(meal) = meal {
            Ok(Some(meal.into()))
        } else {
            Ok(None)
        }
    }

    async fn delete_meal(&self, id: i32) -> Result<(), MenuError> {
        meal::Entity::delete_by_id(id).exec(&self.db).await?;
        Ok(())
    }

    // Category
    async fn get_categories(&self) -> Vec<Category> {
        let cat = category::Entity::find()
            .all(&self.db)
            .await
            .expect("Failed to fetch categories");
        cat.iter().map(|x| x.clone().into()).collect()
    }

    async fn add_category(&self, item: &Category) -> Result<i32, MenuError> {
        let item: category::ActiveModel = item.clone().into();
        let mut item = item.save(&self.db).await?;
        Ok(item.id.take().unwrap())
    }

    async fn delete_category(&self, id: i32) -> Result<(), MenuError> {
        category::Entity::delete_by_id(id).exec(&self.db).await?;
        Ok(())
    }

    async fn get_ingredients(&self) -> Vec<Ingredient> {
        let ing = ingredient::Entity::find()
            .all(&self.db)
            .await
            .expect("Failed to fetch ingredients");
        ing.iter().map(|x| x.clone().into()).collect()
    }

    async fn add_ingredient(&self, item: &Ingredient) -> Result<i32, MenuError> {
        let item: ingredient::ActiveModel = item.clone().into();
        let mut item = item.save(&self.db).await?;
        Ok(item.id.take().unwrap())
    }

    async fn delete_ingredient(&self, id: i32) -> Result<(), MenuError> {
        ingredient::Entity::delete_by_id(id).exec(&self.db).await?;
        Ok(())
    }

    async fn get_all_meal_ingredients(&self) -> Result<Vec<MealIngredient>, MenuError> {
        let items = meal_ingredient::Entity::find().all(&self.db).await?;
        Ok(items.iter().map(|x| x.clone().into()).collect())
    }

    async fn get_meal_ingredients(&self, meal_id: i32) -> Result<Vec<MealIngredient>, MenuError> {
        let items = meal_ingredient::Entity::find()
            .filter(meal_ingredient::Column::MealId.eq(meal_id))
            .all(&self.db)
            .await?;
        Ok(items.iter().map(|x| x.clone().into()).collect())
    }

    async fn delete_meal_ingredient(&self, item: &MealIngredient) -> Result<(), MenuError> {
        let item: meal_ingredient::ActiveModel = item.clone().into();
        item.clone().delete(&self.db).await?;
        Ok(())
    }

    async fn add_meal_ingredient(&self, item: &MealIngredient) -> Result<(), MenuError> {
        let item: meal_ingredient::ActiveModel = item.clone().into();
        let status = item.clone().insert(&self.db).await;
        if status.is_err() {
            item.save(&self.db).await?;
        }
        Ok(())
    }

    async fn get_ingredient(&self, id: i32) -> Result<Ingredient, MenuError> {
        let item = ingredient::Entity::find_by_id(id).one(&self.db).await?;
        if let Some(item) = item {
            Ok(item.into())
        } else {
            Err(MenuError::MenuStr("Ingredient not found"))
        }
    }
}
