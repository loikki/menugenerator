use rocket::serde::json::Json;
use rocket::tokio::sync::Mutex;
use rocket::{delete, get, put, routes, Route, State};

use crate::api::Category;
use crate::config::Config;
use crate::database::Database;
use crate::database_trait::DatabaseTrait;
use crate::menu_error::MenuError;

pub fn get_routes() -> Vec<Route> {
    routes![get_categories, add_category, delete_category]
}

#[get("/")]
pub async fn get_categories(
    config: &State<Mutex<Config>>,
) -> Result<Json<Vec<Category>>, MenuError> {
    let config = config.inner().lock().await;
    // Just fetch the data from the db
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;
    Ok(Json(db.get_categories().await))
}

#[put("/", data = "<category>")]
pub async fn add_category(
    category: Json<Category>,
    config: &State<Mutex<Config>>,
) -> Result<Json<i32>, MenuError> {
    let config = config.inner().lock().await;
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;
    let category = category.into_inner();
    Ok(Json(db.add_category(&category).await?))
}

#[delete("/<id>")]
pub async fn delete_category(id: i32, config: &State<Mutex<Config>>) -> Result<(), MenuError> {
    let config = config.inner().lock().await;
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;
    db.delete_category(id).await
}
