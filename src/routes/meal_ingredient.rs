use rocket::serde::json::Json;
use rocket::tokio::sync::Mutex;
use rocket::{delete, get, post, put, routes, Route, State};

use crate::api::MealIngredient;
use crate::config::Config;
use crate::database::Database;
use crate::database_trait::DatabaseTrait;
use crate::menu_error::MenuError;

pub fn get_routes() -> Vec<Route> {
    routes![
        get_meal_ingredients,
        get_all_meal_ingredients,
        add_meal_ingredient,
        delete_meal_ingredient,
        recompute_seasons
    ]
}

#[get("/<meal_id>")]
pub async fn get_meal_ingredients(
    config: &State<Mutex<Config>>,
    meal_id: i32,
) -> Result<Json<Vec<MealIngredient>>, MenuError> {
    let config = config.inner().lock().await;
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;
    let meal_ingredients = db.get_meal_ingredients(meal_id).await?;
    Ok(Json(meal_ingredients))
}

#[get("/")]
pub async fn get_all_meal_ingredients(
    config: &State<Mutex<Config>>,
) -> Result<Json<Vec<MealIngredient>>, MenuError> {
    let config = config.inner().lock().await;
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;
    let meal_ingredients = db.get_all_meal_ingredients().await?;
    Ok(Json(meal_ingredients))
}

#[put("/", data = "<meal_ingredient>")]
pub async fn add_meal_ingredient(
    meal_ingredient: Json<MealIngredient>,
    config: &State<Mutex<Config>>,
) -> Result<(), MenuError> {
    let config = config.inner().lock().await;
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;
    let meal_ingredient = meal_ingredient.into_inner();
    db.add_meal_ingredient(&meal_ingredient).await?;

    let meal = db
        .get_meal_by_id(meal_ingredient.meal_id)
        .await?
        .expect("Should not happen");
    db.recompute_seasons_single_meal(meal).await?;
    Ok(())
}

#[delete("/ingredient", data = "<meal_ingredient>")]
pub async fn delete_meal_ingredient(
    meal_ingredient: Json<MealIngredient>,
    config: &State<Mutex<Config>>,
) -> Result<(), MenuError> {
    let config = config.inner().lock().await;
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;
    db.delete_meal_ingredient(&meal_ingredient).await
}

#[post("/recompute-seasons")]
pub async fn recompute_seasons(config: &State<Mutex<Config>>) -> Result<(), MenuError> {
    let config = config.inner().lock().await;
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;
    db.recompute_seasons().await
}
