use std::collections::BTreeMap;

use rocket::tokio::sync::Mutex;
use rocket::{post, routes, Route, State};

use crate::api::{Category, Ingredient, Meal, MealIngredient};
use crate::config::Config;
use crate::database::Database;
use crate::database_trait::DatabaseTrait;
use crate::menu_error::MenuError;

pub fn get_routes() -> Vec<Route> {
    routes![pull]
}

#[macro_export]
macro_rules! add_and_get_mapping {
    ($items:ident, $db:ident, $get_by_name:ident, $add:ident) => {{
        let mut mapping: BTreeMap<i32, i32> = BTreeMap::new();

        for item in &$items {
            let item_in_db = $db.$get_by_name(&item.name).await?;
            let new_id = if let Some(item_in_db) = item_in_db {
                item_in_db.id.unwrap()
            } else {
                let mut copy = item.clone();
                copy.id = None;
                $db.$add(&copy).await?
            };

            mapping.insert(item.id.unwrap(), new_id);
        }

        mapping
    }};
}

#[post("/pull/<address>")]
pub async fn pull(config: &State<Mutex<Config>>, address: &str) -> Result<(), MenuError> {
    let categories: Vec<Category> = reqwest::get(format!("{address}/category"))
        .await?
        .json()
        .await?;
    let mut meals: Vec<Meal> = reqwest::get(format!("{address}/meal"))
        .await?
        .json()
        .await?;
    let ingredients: Vec<Ingredient> = reqwest::get(format!("{address}/ingredient"))
        .await?
        .json()
        .await?;
    let mut meal_ingredients: Vec<MealIngredient> =
        reqwest::get(format!("{address}/meal-ingredient"))
            .await?
            .json()
            .await?;

    let config = config.inner().lock().await;
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;

    // Add the categories
    let categories_mapping =
        add_and_get_mapping!(categories, db, get_category_by_name, add_category);

    // Update the references to the meals
    meals.iter_mut().for_each(|x| {
        x.category_id = categories_mapping[&x.category_id];
    });

    // Add the meals
    let meals_mapping = add_and_get_mapping!(meals, db, get_meal_by_name, add_meal);

    // Add the ingredients
    let ingredient_mapping =
        add_and_get_mapping!(ingredients, db, get_ingredient_by_name, add_ingredient);

    // Update references in meal_ingredients
    meal_ingredients.iter_mut().for_each(|x| {
        x.ingredient_id = ingredient_mapping[&x.ingredient_id];
        x.meal_id = meals_mapping[&x.meal_id];
    });
    Ok(())
}
