use rocket::serde::json::Json;
use rocket::tokio::sync::Mutex;
use rocket::{delete, get, put, routes, Route, State};

use crate::api::Meal;
use crate::config::Config;
use crate::database::Database;
use crate::database_trait::DatabaseTrait;
use crate::menu_error::MenuError;

pub fn get_routes() -> Vec<Route> {
    routes![get_meals, get_meals_in_month, add_meal, delete_meal,]
}

#[get("/")]
pub async fn get_meals(config: &State<Mutex<Config>>) -> Result<Json<Vec<Meal>>, MenuError> {
    let config = config.inner().lock().await;
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;
    Ok(Json(db.get_meals().await))
}

#[get("/month/<month>")]
pub async fn get_meals_in_month(
    month: u8,
    config: &State<Mutex<Config>>,
) -> Result<Json<Vec<Meal>>, MenuError> {
    let config = config.inner().lock().await;
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;
    Ok(Json(db.get_meals_in_month(month).await))
}

#[put("/", data = "<meal>")]
pub async fn add_meal(
    meal: Json<Meal>,
    config: &State<Mutex<Config>>,
) -> Result<Json<i32>, MenuError> {
    let config = config.inner().lock().await;
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;
    let meal = meal.into_inner();
    let id = db.add_meal(&meal).await?;
    Ok(Json(id))
}

#[delete("/<id>")]
pub async fn delete_meal(id: i32, config: &State<Mutex<Config>>) -> Result<(), MenuError> {
    let config = config.inner().lock().await;
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;
    db.delete_meal(id).await
}
