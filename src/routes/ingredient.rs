use rocket::serde::json::Json;
use rocket::tokio::sync::Mutex;
use rocket::{delete, get, put, routes, Route, State};

use crate::api::Ingredient;
use crate::config::Config;
use crate::database::Database;
use crate::database_trait::DatabaseTrait;
use crate::menu_error::MenuError;

pub fn get_routes() -> Vec<Route> {
    routes![get_ingredients, add_ingredient, delete_ingredient]
}

#[get("/")]
pub async fn get_ingredients(
    config: &State<Mutex<Config>>,
) -> Result<Json<Vec<Ingredient>>, MenuError> {
    let config = config.inner().lock().await;
    // Just fetch the data from the db
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;
    Ok(Json(db.get_ingredients().await))
}

#[put("/", data = "<ingredient>")]
pub async fn add_ingredient(
    ingredient: Json<Ingredient>,
    config: &State<Mutex<Config>>,
) -> Result<Json<i32>, MenuError> {
    let config = config.inner().lock().await;
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;
    let ingredient = ingredient.into_inner();
    Ok(Json(db.add_ingredient(&ingredient).await?))
}

#[delete("/<id>")]
pub async fn delete_ingredient(id: i32, config: &State<Mutex<Config>>) -> Result<(), MenuError> {
    let config = config.inner().lock().await;
    let url = config.database_url().clone();
    let db = Database::new(&url).await?;
    db.delete_ingredient(id).await
}
