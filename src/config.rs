use serde::{Deserialize, Serialize};
use std::path::Path;
use std::str;
use std::{fs::File, path::PathBuf};

use crate::menu_error::MenuError;

const CONFIG_FILE: &str = "config.json";

#[derive(Serialize, Deserialize)]
pub struct Config {
    database_url: String,
    expose_rocket: bool,
    frontend_path: String,
    config_dir: String,
}

impl Config {
    pub fn new(config_dir: &Path) -> Self {
        let config = config_dir.to_str().unwrap();
        let default = Self {
            frontend_path: "frontend/build".to_string(),
            expose_rocket: false,
            database_url: format!("sqlite://{config}/db.sqlite?mode=rwc"),
            config_dir: config.to_string(),
        };

        let config_file = config_dir.join(CONFIG_FILE);
        let file = match File::open(config_file) {
            Ok(f) => f,
            Err(_) => {
                default.save().unwrap();
                return default;
            }
        };

        let config = match serde_json::from_reader(file) {
            Err(_) => default,
            Ok(c) => {
                println!("Successfully read configuration file");
                c
            }
        };
        config.save().unwrap();
        config
    }

    pub fn save(&self) -> Result<(), MenuError> {
        let filename = PathBuf::from(self.config_dir.clone()).join(CONFIG_FILE);
        let file = File::create(filename)?;
        serde_json::to_writer(file, self)?;
        Ok(())
    }

    pub fn expose_rocket(&self) -> bool {
        self.expose_rocket
    }

    pub fn database_url(&self) -> &String {
        &self.database_url
    }

    pub fn frontend_path(&self) -> &String {
        &self.frontend_path
    }

    pub fn config_dir(&self) -> &String {
        &self.config_dir
    }
}
