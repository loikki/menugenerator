use crate::{
    api::{Category, Ingredient, Meal, MealIngredient},
    menu_error::MenuError,
};
use async_trait::async_trait;

fn compute_meal_season(ingredients: &[Ingredient]) -> (u8, u8) {
    if ingredients.is_empty() {
        panic!("This function should not be used with empty arguments");
    }
    let mut start = ingredients[0].month_start;
    let mut end = ingredients[0].month_end;
    ingredients.iter().skip(1).for_each(|x| {
        // Case where both intervals are "normal" or "cross new year"
        if (start < end && x.month_start < x.month_end)
            || (start > end && x.month_start > x.month_end)
        {
            start = std::cmp::max(start, x.month_start);
            end = std::cmp::min(end, x.month_end);
        }
        // Mix case
        else if start < x.month_end {
            end = x.month_end;
        } else {
            start = x.month_start;
        }
    });
    (start, end)
}

#[async_trait]
pub trait DatabaseTrait {
    async fn get_meals(&self) -> Vec<Meal>;
    async fn get_meals_in_month(&self, current: u8) -> Vec<Meal> {
        let meals = self.get_meals().await;
        #[allow(clippy::nonminimal_bool)]
        meals
            .iter()
            .filter(|x| {
                // start <= current <= end
                (x.month_start <= current && current <= x.month_end) ||
            //  current <= end <= start
                (current <= x.month_end && x.month_end <= x.month_start) ||
            //  current >= start >= end
                (current >= x.month_start && x.month_start >= x.month_end)
            })
            .cloned()
            .collect()
    }

    async fn add_meal(&self, meal: &Meal) -> Result<i32, MenuError>;
    async fn get_meal_by_name(&self, name: &str) -> Result<Option<Meal>, MenuError>;
    async fn get_meal_by_id(&self, id: i32) -> Result<Option<Meal>, MenuError>;
    async fn delete_meal(&self, id: i32) -> Result<(), MenuError>;
    async fn get_categories(&self) -> Vec<Category>;
    async fn add_category(&self, category: &Category) -> Result<i32, MenuError>;
    async fn delete_category(&self, id: i32) -> Result<(), MenuError>;
    async fn get_ingredients(&self) -> Vec<Ingredient>;
    async fn get_ingredient(&self, id: i32) -> Result<Ingredient, MenuError>;
    async fn add_ingredient(&self, ingredient: &Ingredient) -> Result<i32, MenuError>;
    async fn delete_ingredient(&self, id: i32) -> Result<(), MenuError>;
    async fn get_all_meal_ingredients(&self) -> Result<Vec<MealIngredient>, MenuError>;
    async fn get_meal_ingredients(&self, meal_id: i32) -> Result<Vec<MealIngredient>, MenuError>;
    async fn add_meal_ingredient(&self, item: &MealIngredient) -> Result<(), MenuError>;
    async fn delete_meal_ingredient(&self, item: &MealIngredient) -> Result<(), MenuError>;
    async fn recompute_seasons_single_meal(&self, mut meal: Meal) -> Result<(), MenuError> {
        // TODO cleanup unwrap / expect
        let meal_ingredients = self.get_meal_ingredients(meal.id.unwrap()).await?;
        if meal_ingredients.is_empty() {
            return Ok(());
        }

        // TODO avoid multiple queries
        let mut ings = vec![];
        for item in &meal_ingredients {
            ings.push(self.get_ingredient(item.ingredient_id).await?);
        }

        (meal.month_start, meal.month_end) = compute_meal_season(&ings);
        self.add_meal(&meal).await?;
        Ok(())
    }
    async fn recompute_seasons(&self) -> Result<(), MenuError> {
        let meals = self.get_meals().await;
        // TODO use async iterator
        for meal in &meals {
            self.recompute_seasons_single_meal(meal.clone()).await?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::api::Ingredient;

    use super::compute_meal_season;

    #[test]
    fn season_based_on_ingredients() {
        // Normal case
        let ing1 = Ingredient {
            month_start: 0,
            month_end: 6,
            ..Default::default()
        };
        let ing2 = Ingredient {
            month_start: 2,
            month_end: 8,
            ..Default::default()
        };
        let (start, end) = compute_meal_season(&vec![ing1, ing2]);
        assert_eq!(start, 2);
        assert_eq!(end, 6);

        // End of year case
        let ing1 = Ingredient {
            month_start: 6,
            month_end: 0,
            ..Default::default()
        };
        let ing2 = Ingredient {
            month_start: 9,
            month_end: 4,
            ..Default::default()
        };
        let (start, end) = compute_meal_season(&vec![ing1, ing2]);
        assert_eq!(start, 9);
        assert_eq!(end, 0);

        // First: normal case, second end of year
        let ing1 = Ingredient {
            month_start: 1,
            month_end: 6,
            ..Default::default()
        };
        let ing2 = Ingredient {
            month_start: 9,
            month_end: 4,
            ..Default::default()
        };
        let (start, end) = compute_meal_season(&vec![ing1, ing2]);
        assert_eq!(start, 1);
        assert_eq!(end, 4);

        // First: end of year, second normal
        let ing1 = Ingredient {
            month_start: 6,
            month_end: 0,
            ..Default::default()
        };
        let ing2 = Ingredient {
            month_start: 1,
            month_end: 11,
            ..Default::default()
        };
        let (start, end) = compute_meal_season(&vec![ing1, ing2]);
        assert_eq!(start, 6);
        assert_eq!(end, 11);
    }
}
