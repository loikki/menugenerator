use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Ingredient
        manager
            .create_table(
                Table::create()
                    .table(Ingredient::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Ingredient::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key()
                    )
                    .col(
                        ColumnDef::new(Ingredient::Name)
                            .string()
                            .not_null()
                            .unique_key()
                    )
                    .col(
                        ColumnDef::new(Ingredient::MonthStart)
                            .tiny_unsigned()
                            .not_null()
                    )
                    .col(
                        ColumnDef::new(Ingredient::MonthEnd)
                            .tiny_unsigned()
                            .not_null()
                    )
                    .col(
                        ColumnDef::new(Ingredient::Unit)
                            .string()
                            .not_null()
                    )
                    .to_owned(),
            )
            .await?;

        // Index
        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_ingredient_name")
                    .table(Ingredient::Table)
                    .col(Ingredient::Name)
                    .to_owned()
            ).await?;

        // Meal Ingredient
        manager
            .create_table(
                Table::create()
                    .table(MealIngredient::Table)
                    .if_not_exists()
                    .primary_key(
                        Index::create()
                            .col(MealIngredient::IngredientId)
                            .col(MealIngredient::MealId)
                    )
                    .col(
                        ColumnDef::new(MealIngredient::IngredientId)
                            .integer()
                            .not_null()
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .name("ingredient_id")
                            .from(MealIngredient::Table, MealIngredient::IngredientId)
                            .to(Ingredient::Table, Ingredient::Id)
                    )
                    .col(
                        ColumnDef::new(MealIngredient::MealId)
                            .integer()
                            .not_null()
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .name("meal_id")
                            .from(MealIngredient::Table, MealIngredient::MealId)
                            .to(Meal::Table, Meal::Id)
                    )
                    .col(
                        ColumnDef::new(MealIngredient::Quantity)
                            .float()
                            .not_null()
                    )
                    .to_owned(),
            ).await?;

        // Add recipes
        manager.alter_table(
            Table::alter()
                .table(Meal::Table)
                .add_column(
                    ColumnDef::new(Meal::Recipe)
                        .text()
                        .not_null()
                )
                .to_owned()
        ).await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Meal Ingredient
        manager
            .drop_table(Table::drop().table(MealIngredient::Table).to_owned())
            .await?;

        // Index
        manager
            .drop_index(Index::drop().name("idx_ingredient_name").to_owned())
            .await?;

        // Meal
        manager
            .drop_table(Table::drop().table(Meal::Table).to_owned())
            .await?;

        // Remove recipes
        manager
            .alter_table(
                Table::alter()
                    .table(Meal::Table)
                    .drop_column(Meal::Recipe)
                    .to_owned()
            )
            .await
    }
}

#[derive(DeriveIden)]
enum Ingredient {
    Table,
    Id,
    Name,
    MonthStart,
    MonthEnd,
    Unit,
}

#[derive(DeriveIden)]
enum MealIngredient {
    Table,
    IngredientId,
    MealId,
    Quantity,
}

#[derive(DeriveIden)]
enum Meal {
    Table,
    Id,
    Recipe,
}
