use sea_orm::EnumIter;
use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Category
        manager
            .create_table(
                Table::create()
                    .table(Category::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Meal::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key()
                        )
                    .col(
                        ColumnDef::new(Meal::Name)
                            .string()
                            .not_null()
                            .unique_key()
                    )
                    .to_owned()
            ).await?;

        // Index
        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_category_name")
                    .table(Category::Table)
                    .col(Category::Name)
                    .to_owned()
            ).await?;

        // Meal
        manager
            .create_table(
                // Meal
                Table::create()
                    .table(Meal::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Meal::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key()
                    )
                    .col(
                        ColumnDef::new(Meal::Name)
                            .string()
                            .not_null()
                            .unique_key()
                    )
                    .col(
                        ColumnDef::new(Meal::Light)
                            .not_null()
                            .boolean()
                    )
                    .col(
                        ColumnDef::new(Meal::Meat)
                            .not_null()
                            .boolean()
                    )
                    .col(
                        ColumnDef::new(Meal::MonthStart)
                            .not_null()
                            .tiny_unsigned()
                    )
                    .col(
                        ColumnDef::new(Meal::MonthEnd)
                            .not_null()
                            .tiny_unsigned()
                    )
                    .col(
                        ColumnDef::new(Meal::Source)
                            .not_null()
                            .string()
                    )
                    .col(
                        ColumnDef::new(Meal::CategoryId)
                            .not_null()
                            .integer()
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .name("category_id")
                            .from(Meal::Table, Meal::CategoryId)
                            .to(Category::Table, Category::Id)
                    )
                    .to_owned()
            ).await?;

        // Index
        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_meal_name")
                    .table(Meal::Table)
                    .col(Meal::Name)
                    .to_owned()
            ).await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Index
        manager
            .drop_index(Index::drop().name("idx_meal_name").to_owned())
            .await?;

        // Meal
        manager
            .drop_table(Table::drop().table(Meal::Table).to_owned())
            .await?;

        // Index
        manager
            .drop_index(Index::drop().name("idx_category_name").to_owned())
            .await?;

        // Category
        manager
            .drop_table(Table::drop().table(Category::Table).to_owned())
            .await
    }
}

#[derive(DeriveIden)]
enum Meal {
    Table,
    Id,
    Name,
    Light,
    Meat,
    MonthStart,
    MonthEnd,
    Source,
    CategoryId,
}

#[derive(Iden, EnumIter)]
enum Category {
    Table,
    Id,
    Name
}
