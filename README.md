# MenuGenerator
[![discord](https://img.shields.io/badge/Discord-7289DA)](https://discord.gg/ZFstbnfppZ)
[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="40">](https://f-droid.org/packages/io.menu_generator/)

This repository contains a web service to manage a list of meal according to the season.


## Screenshots

![meals](fastlane/metadata/android/en-US/images/phoneScreenshots/meals.png){width=25% height=25%}
![categories](fastlane/metadata/android/en-US/images/phoneScreenshots/categories.png){width=25% height=25%}
